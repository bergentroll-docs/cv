# Curriculum vitae

![build
status](https://gitlab.com/bergentroll-docs/cv/badges/master/pipeline.svg?ignore_skipped=true&job=wup&key_width=40&key_text=build)

- [Резюме (русский)](https://bergentroll-docs.gitlab.io/cv/karmanov_cv_ru.pdf)
- [CV (English)](https://bergentroll-docs.gitlab.io/cv/karmanov_cv_en.pdf)
